#!/bin/bash

menu()
{
	echo "---------------------------------------"
	echo -e "\t\tMENU"	
	echo "---------------------------------------"
	echo "1) selecionar um arquivo"
	echo "2) fazer um resumo do arquivo selecionado"
	echo "3) exibir o arquivo selecionado"
	echo "4) criar um arquivo com n linhas de blablabla"
	echo "5) pesquisar algo no arquivo selecionado"
	echo "6) criar uma cópia do arquivo selecionado"
	echo "7) sair"

}

while [ "$opc" != "7" ]; do
	menu
	read opc
	case $opc in
		1) 
			read -p "Diga o nome de um arquivo" arq
			x=$arq
			;;
		2)
			[ -f $x ] && head -n 3 $x | tail -n 3 || echo "O arquivo selecionado não existe"
			;;
		3)
			echo "Exibindo o conteudo do arquivo selecionado: $(cat $x)"
			;;
		4)
			read -p "Diga a quantidade de linhas do arquivo" linhas
			./gerar-arq.sh $linhas
			;;
		5)
			read -p "Diga a palavra que deseja pesquisar" plv
			echo "Pesquisando sua palavra..."
			sleep 3
			cat $x | grep $plv || echo "Ñ foi possivel encontrar a sua palavra"
			;;
		6)	
			cp $x $x.copia && echo "Arquivo copiado com sucesso!"
			;;
		7) 
			echo "saindo..."
			sleep 3
			;;

	esac
done

